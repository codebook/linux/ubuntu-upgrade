# How to Upgrade Ubuntu Releases

_Guide to upgrading Ubuntu 16.04 LTS to 17.04 and then to 17.10 (beta)_


First, note that LTS releases can normally be upgraded to LTS releases only.
In order to upgrade LTS to a newer short-term release, you will need to change your software upgrade settings.

Open Software and Updates. In the Updates tab, change the value of the "Notify me of a new Ubuntu version" setting to "For any new version".


Open a terminal and do apt update and upgrade:

    sudo apt update && sudo apt full-upgrade -y


Then, run the release upgrade command:

    sudo do-release-upgrade


You can first check what release version your system can be upgraded to by using `-c` flag:

    sudo do-release-upgrade -c



For example, I had 16.04.3 LTS on one of my computers, and I upgraded it to 17.04 this way.

Then, I ran the release upgrade command again to upgrade it to 17.10 (beta) using `-d` flag (for "devel-release"):


    sudo do-release-upgrade -d



Note: After each upgrade, I was having issues with `apt` commands:

> Reading package lists... Error!                   
E: Encountered a section with no Package: header
E: Problem with MergeList /var/lib/apt/lists/us.archive.ubuntu.com_ubuntu_dists_zesty_universe_binary-amd64_Packages
E: The package lists or status file could not be parsed or opened.

And, I had to clean up the package lists before running `apt update`.

    sudo rm -f /var/lib/apt/lists/*



_(Note: Ubuntu 17.10 (Aardvark) is scheduled to be released in a couple of weeks (10/19/2017) as of this writing.)_


